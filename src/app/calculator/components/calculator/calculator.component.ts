import { CalculatorService } from '../../services';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {

  listBtnId = [['7', '8', '9', '/'], ['4', '5', '6', '*'], ['1', '2', '3', '-'], ['0', '.', '=', '+']];
  private number1: string;
  private number2: string;
  private result: number;
  private operation: string;

  constructor(private calculatorService: CalculatorService) {}

  ngOnInit() {
    this.clean();
  }

  /**
   * initializes all operators to default values
   * 
   * @return void
   */
  clean(): void {
    this.number1 = '0';
    this.number2 = null;
    this.result = null;
    this.operation = null;
  }

  /**
   * Adds the selected number to the calculation later.
   *
   * @param string number
   * @return void
   */
  addNumber(number: string): void {
  	if (this.operation === null) {
  	  this.number1 = this.concatNumber(this.number1, number);
  	} else {
  	  this.number2 = this.concatNumber(this.number2, number);
  	}
  }

  selectAction(option: string): void {
    if (['.', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'].indexOf(option) !== -1) {
      this.addNumber(option);
    } else if (['+', '-', '/', '*'].indexOf(option) !== -1) {
      this.defineOperation(option);
    } else if (option === '=') {
      this.calculate();
    }
  }

  /**
   * Returns the concatenated value. Treats the decimal separator.
   *
   * @param string currentNumber
   * @param string concatNumber
   * @return string
   */
  concatNumber(currentNumber: string, concatNumber: string): string {
  	// if it contains only '0' or null, restarts the value
    if (currentNumber === '0' || currentNumber === null) {
  	  currentNumber = '';
  	}

    // first digit is '.', concatenate '0' before dot
  	if (concatNumber === '.' && currentNumber === '') {
  	  return '0.';
  	}

    // case '.' typed and already contains a '.', just returns
  	if (concatNumber === '.' && currentNumber.indexOf('.') > -1) {
  	  return currentNumber;
  	}

  	return currentNumber + concatNumber;
  }

  /**
   * Performs logic when an operator is selected.
   * If you already have an operation selected, perform
   * the previous operation and define the new operation.
   *
   * @param string operation
   * @return void
   */
  defineOperation(operation: string): void {
    // only defines the operation if there is no
  	if (this.operation === null) {
      this.operation = operation;
      return;
  	}

    /* if operation set and number 2 selected,
        performs the operation calculation */
  	if (this.number2 !== null) {
  		this.result = this.calculatorService.calculate(
  			parseFloat(this.number1), 
  			parseFloat(this.number2), 
  			this.operation);
  		this.operation = operation;
  		this.number1 = this.result.toString();
  		this.number2 = null;
  		this.result = null;
  	}
  }

  /**
   * Performs the calculation of an operation.
   *
   * @return void
   */
  calculate(): void {
  	if (this.number2 === null) {
  		return;
  	}

  	this.result = this.calculatorService.calculate(
  		parseFloat(this.number1), 
  		parseFloat(this.number2), 
      this.operation);
  }

  /**
   * Returns the value to be displayed on the calculator screen.
   *
   * @return string
   */
  get display(): string {
  	if (this.result !== null) {
  		return this.result.toString();
  	}
  	if (this.number2 !== null) {
  		return this.number2;
  	}
  	return this.number1;
  }

}
