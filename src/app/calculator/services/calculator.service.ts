import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculatorService {

  /* Constantes utilizadas para identificar as operações
     de cálculo */
  static readonly ADD: string = '+';
  static readonly SUBTRACTION: string = '-';
  static readonly DIVISION: string = '/';
  static readonly MULTIPLICATION: string = '*';

  constructor() { }

  /**
   * Calcular operação matemática
   * @param num1 first number
   * @param num2 second number
   * @param operation string operation to be executed
   * @return Return number as result
   */
  calculate(num1: number, num2: number, operation: string): number {
    let result: number; // save the operation result

    switch(operation) {
      case CalculatorService.ADD:
        result = num1 + num2;
      break
      case CalculatorService.SUBTRACTION:
        result = num1 - num2;
      break
      case CalculatorService.DIVISION:
        result = num1 / num2;
      break
      case CalculatorService.MULTIPLICATION:
        result = num1 * num2;
      break
      default:
        result = 0;
    }

    return result;
  }
}
