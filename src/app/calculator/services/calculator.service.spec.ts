import { TestBed, inject } from '@angular/core/testing';

import { CalculatorService } from './';

describe('CalculatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CalculatorService = TestBed.get(CalculatorService);
    expect(service).toBeTruthy();
  });

  it('Try add values',
    inject([CalculatorService], (service: CalculatorService) => {
      let resAdd = service.calculate(1, 5, CalculatorService.ADD);
      expect(resAdd).toEqual(6);
    })
  );

  it('Try subtract values',
    inject([CalculatorService], (service: CalculatorService) => {
      let resAdd = service.calculate(2, 7, CalculatorService.SUBTRACTION);
      expect(resAdd).toEqual(-5);
    })
  );
  
  it('Try divide values',
    inject([CalculatorService], (service: CalculatorService) => {
      let resAdd = service.calculate(6, 3, CalculatorService.DIVISION);
      expect(resAdd).toEqual(2);
    })
  );
  
  it('Try multiply values',
    inject([CalculatorService], (service: CalculatorService) => {
      let resAdd = service.calculate(6, 3, CalculatorService.MULTIPLICATION);
      expect(resAdd).toEqual(18);
    })
  );
  
  it('Try diferent operation',
    inject([CalculatorService], (service: CalculatorService) => {
      let resAdd = service.calculate(6, 3, '%');
      expect(resAdd).toEqual(0);
    })
  );

});
